// Copyright 2013 George McBay (george.mcbay@gmail.com)
// All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package watch

import (
	"log"
	"reflect"
	"sync"
)

type OnChanged func()

type watchState struct {
	value         reflect.Value
	previousValue interface{}
	onChangedList []OnChanged
}

type Watcher struct {
	sync.RWMutex
	watched map[reflect.Value]map[string]*watchState
}

func NewWatcher() (watcher *Watcher) {
	watcher = new(Watcher)
	watcher.watched = make(map[reflect.Value]map[string]*watchState)
	return
}

func (watcher *Watcher) Add(obj interface{}, name string, onChanged OnChanged) {
	watcher.Lock()
	defer watcher.Unlock()

	var ptrValue reflect.Value
	var previousValue interface{}
	var item *watchState
	var watchedObj map[string]*watchState
	var exists bool

	objValue := reflect.ValueOf(obj)

	if objValue.Kind() == reflect.Ptr {
		ptrValue = objValue
		objValue = objValue.Elem()
	}

	if objValue.Kind() != reflect.Struct {
		log.Panicf("watch: Invalid type passed to Watcher.Add: %#v\n", objValue)
	}

	watchedValue := objValue.FieldByName(name)
	elem := objValue.Addr().Elem()

	// If we don't find a field with this name on the object, look for a method
	if watchedValue.Kind() == reflect.Invalid {
		typeOf := reflect.TypeOf(obj)

		if typeOf.Kind() == reflect.Invalid {
			log.Panicf("watch: Invalid type passed to Watcher.Add: %#v\n", objValue)
		} else {
			if watchedValue = ptrValue.MethodByName(name); watchedValue.Kind() != reflect.Func ||
				watchedValue.Type().NumIn() != 0 {
				log.Panicf("watch: No Field or suitable Method '%v' found on type: %v\n", name, objValue)
			}

			previousValue = watchedValue.Call(nil)[0].Interface()
		}
	} else {
		previousValue = watchedValue.Interface()
	}

	if watchedObj, exists = watcher.watched[elem]; !exists {
		watchedObj = make(map[string]*watchState)
		watcher.watched[elem] = watchedObj
	}

	// if this field instance doesn't already exist, create it and place
	// it into the watcher list.
	if item, exists = watchedObj[name]; !exists {
		item = new(watchState)
		item.value = watchedValue
		item.previousValue = previousValue
		item.onChangedList = make([]OnChanged, 0)
		watchedObj[name] = item
	}

	// add new OnChanged function to list
	item.onChangedList = append(item.onChangedList, onChanged)

	// call the onChanged
	onChanged()

	return
}

func (watcher *Watcher) getWatchedObj(obj interface{}) (elem reflect.Value) {
	objValue := reflect.ValueOf(obj)

	if objValue.Kind() == reflect.Ptr {
		objValue = objValue.Elem()
	}

	if objValue.IsValid() {
		elem = objValue.Addr().Elem()
	}

	return
}

func (watcher *Watcher) Remove(obj interface{}, name string) {
	watcher.Lock()
	defer watcher.Unlock()

	elem := watcher.getWatchedObj(obj)

	if elem.IsValid() {
		if elemObj, exists := watcher.watched[elem]; exists {
			if _, exists := elemObj[name]; exists {
				delete(elemObj, name)
			}
		}
	}
}

func (watcher *Watcher) RemoveAll(obj interface{}) {
	watcher.Lock()
	defer watcher.Unlock()

	elem := watcher.getWatchedObj(obj)

	if elem.IsValid() {
		if _, exists := watcher.watched[elem]; exists {
			delete(watcher.watched, elem)
		}
	}
}

func (watcher *Watcher) Update() (changes int) {
	watcher.RLock()
	defer watcher.RUnlock()

	var currentValue interface{}

	// Loop through watched items, report any changes
	for _, items := range watcher.watched {
		for _, state := range items {
			if state.value.Kind() == reflect.Func {
				currentValue = state.value.Call(nil)[0].Interface()
			} else {
				currentValue = state.value.Interface()
			}

			if currentValue != state.previousValue {
				// value has changed since the last update,
				// inform all registered onChanged functions
				for _, onChanged := range state.onChangedList {
					changes++
					onChanged()
				}

				// mark new value
				state.previousValue = currentValue
			}
		}
	}

	return
}
